use std::fs;

fn map_num(v: &str) -> i32 {
	if v == "A" || v == "X" {
		return 1;
	} else if v == "B" || v == "Y" {
		return 2;
	} else if v == "C" || v == "Z" {
		return 3;
	}

	return 0;
}

pub fn pt1() {
	let input = fs::read_to_string("inputs/day2.txt").unwrap();

	let mut score = 0;

	for (opponent, you) in input.lines().map(|line| {
		let (a, b) = line.split_once(' ').unwrap();
		(map_num(a), map_num(b))
	}) {
		score += you;

		if you == opponent {
			// Draw
			score += 3;
		} else if opponent == you - 1 || (you == 1 && opponent == 3) {
			// You win
			score += 6;
		}
	}

	println!("{}", score);
}

pub fn pt2() {
	let input = fs::read_to_string("inputs/day2.txt").unwrap();

	let mut score = 0;

	for (opponent, target) in input.lines().map(|line| {
		let (a, b) = line.split_once(' ').unwrap();
		(map_num(a), map_num(b))
	}) {
		let you;
		if target == 1 {
			// Lose
			if opponent == 1 {
				you = 3;
			} else {
				you = opponent - 1;
			}
		} else if target == 2 {
			// Draw
			score += 3;
			you = opponent;
		} else {
			// Win
			score += 6;
			if opponent == 3 {
				you = 1;
			} else {
				you = opponent + 1;
			}
		}
		score += you;
	}

	println!("{}", score);
}
