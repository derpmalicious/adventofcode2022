use std::collections::HashSet;
use std::fs;

pub fn pt1() {
	let input = fs::read_to_string("inputs/day9.txt").unwrap();

	let mut visited = HashSet::new();
	let mut head = (0, 0);
	let mut tail = (0, 0);

	visited.insert(tail);

	for line in input.lines() {
		let (direction_str, count_str) = line.split_once(' ').unwrap();
		let count = count_str.parse::<i32>().unwrap();

		let mv = match direction_str {
			"R" => (count, 0),
			"U" => (0, -count),
			"L" => (-count, 0),
			"D" => (0, count),
			_ => (0, 0),
		};

		// Move head
		head.0 += mv.0;
		head.1 += mv.1;

		// Move tail
		while (head.0 - tail.0).abs() > 1 || (head.1 - tail.1).abs() > 1 {
			let mv = ((head.0 - tail.0).clamp(-1, 1), (head.1 - tail.1).clamp(-1, 1));
			tail.0 += mv.0;
			tail.1 += mv.1;

			visited.insert(tail);
		}
	}

	println!("{}", visited.len());
}

pub fn pt2() {
	let input = fs::read_to_string("inputs/day9.txt").unwrap();

	let mut visited = HashSet::new();
	let mut head = (0, 0);
	let mut tails = [(0, 0); 9].to_vec();

	visited.insert(tails[8]);

	for line in input.lines() {
		let (direction_str, count_str) = line.split_once(' ').unwrap();
		let count = count_str.parse::<i32>().unwrap();

		let mv = match direction_str {
			"R" => (count, 0),
			"U" => (0, -count),
			"L" => (-count, 0),
			"D" => (0, count),
			_ => panic!("Nope"),
		};

		// Move head
		head.0 += mv.0;
		head.1 += mv.1;

		// Move tails
		move_tails_towards(head, &mut tails, &mut visited);
	}

	println!("{}", visited.len());
}

fn move_tails_towards(
	target: (i32, i32),
	tails: &mut [(i32, i32)],
	visited: &mut HashSet<(i32, i32)>,
) {
	if tails.len() >= 2 {
		let (this, rest) = tails.split_at_mut(1);
		let mut this = &mut this[0];

		while (target.0 - this.0).abs() > 1 || (target.1 - this.1).abs() > 1 {
			this.0 += (target.0 - this.0).clamp(-1, 1);
			this.1 += (target.1 - this.1).clamp(-1, 1);

			move_tails_towards(*this, rest, visited);
		}
	} else {
		let mut this = &mut tails[0];

		while (target.0 - this.0).abs() > 1 || (target.1 - this.1).abs() > 1 {
			this.0 += (target.0 - this.0).clamp(-1, 1);
			this.1 += (target.1 - this.1).clamp(-1, 1);

			visited.insert(*this);
		}
	}
}
