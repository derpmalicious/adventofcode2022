#![feature(iter_array_chunks)]

extern crate core;

mod day12;

fn main() {
	day12::pt2();
}
