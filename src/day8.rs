use std::collections::HashSet;
use std::fs;

pub fn pt1() {
	let input = fs::read_to_string("inputs/day8.txt").unwrap();

	let grid: Vec<Vec<i32>> = input
		.lines()
		.map(|line| line.chars().map(|ch| ch.to_digit(10).unwrap() as i32).collect())
		.collect();

	let mut visible_coords = HashSet::new();

	for (y, row) in grid.iter().enumerate() {
		let mut height = -1;

		// Ltr
		for (x, n) in row.iter().enumerate() {
			if n > &height {
				visible_coords.insert((x, y));
				height = *n;
			}
		}

		// Rtl
		height = -1;
		for (x, n) in row.iter().enumerate().rev() {
			if n > &height {
				visible_coords.insert((x, y));
				height = *n;
			}
		}
	}

	let row_len = grid[0].len();

	for x in 0..row_len {
		let mut height = -1;

		// Top down
		for (y, row) in grid.iter().enumerate() {
			let n = row[x];
			if n > height {
				visible_coords.insert((x, y));
				height = n;
			}
		}

		// Bottom up
		height = -1;
		for (y, row) in grid.iter().enumerate().rev() {
			let n = row[x];
			if n > height {
				visible_coords.insert((x, y));
				height = n;
			}
		}
	}

	println!("{}", visible_coords.len());
}

pub fn pt2() {
	let input = fs::read_to_string("inputs/day8.txt").unwrap();

	let grid: Vec<Vec<i32>> = input
		.lines()
		.map(|line| line.chars().map(|ch| ch.to_digit(10).unwrap() as i32).collect())
		.collect();

	let rows = grid.len();
	let cols = grid[0].len();

	let mut highest_score = -1;

	for (y, row) in grid.iter().enumerate() {
		for (x, height) in row.iter().enumerate() {
			let mut trees_left = 0;
			for x in (0..x).rev() {
				trees_left += 1;
				if grid[y][x] >= *height {
					break;
				}
			}

			let mut trees_right = 0;
			for x in (x + 1)..cols {
				trees_right += 1;
				if grid[y][x] >= *height {
					break;
				}
			}

			let mut trees_up = 0;
			for y in (0..y).rev() {
				trees_up += 1;
				if grid[y][x] >= *height {
					break;
				}
			}

			let mut trees_down = 0;
			for y in (y + 1)..rows {
				trees_down += 1;
				if grid[y][x] >= *height {
					break;
				}
			}

			let score = trees_left * trees_right * trees_up * trees_down;

			if score > highest_score {
				highest_score = score;
			}
		}
	}

	println!("{highest_score}");
}
