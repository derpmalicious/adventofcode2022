use std::collections::HashSet;
use std::fs;

pub fn pt1() {
	let input = fs::read_to_string("inputs/day6.txt").unwrap();

	for line in input.lines() {
		for (n, buf) in line.chars().collect::<Vec<_>>().windows(4).enumerate() {
			let mut chars = HashSet::new();

			for char in buf {
				chars.insert(char);
			}

			if chars.len() == 4 {
				println!("{}", n + 4);
				break;
			}
		}
	}
}

pub fn pt2() {
	let input = fs::read_to_string("inputs/day6.txt").unwrap();

	for line in input.lines() {
		for (n, buf) in line.chars().collect::<Vec<_>>().windows(14).enumerate() {
			let mut chars = HashSet::new();

			for char in buf {
				chars.insert(char);
			}

			if chars.len() == 14 {
				println!("{}", n + 14);
				break;
			}
		}
	}
}
