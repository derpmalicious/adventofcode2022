use std::cell::RefCell;
use std::collections::HashMap;
use std::fs;
use std::rc::Rc;

struct DirEnt {
	size: usize,
	subentries: HashMap<String, Rc<RefCell<Self>>>,
	parent: Option<Rc<RefCell<Self>>>,
}

impl DirEnt {
	pub fn root() -> Self {
		Self { size: 0, subentries: HashMap::new(), parent: None }
	}

	pub fn add_entry(me: &Rc<RefCell<Self>>, name: &str, size: usize) {
		let parent = me.clone();
		(*me).borrow_mut().subentries.insert(
			name.to_string(),
			Rc::new(RefCell::new(Self { size, subentries: HashMap::new(), parent: Some(parent) })),
		);
	}

	pub fn get_entry(me: &Rc<RefCell<Self>>, name: &str) -> Rc<RefCell<Self>> {
		(*me).borrow().subentries.get(name).unwrap().clone()
	}

	pub fn get_parent(me: &Rc<RefCell<Self>>) -> Rc<RefCell<Self>> {
		let parent = &(*me).borrow().parent;
		parent.as_ref().map_or_else(|| me.clone(), |parent| parent.clone())
	}

	pub fn print_tree(me: &Rc<RefCell<Self>>, level: usize) {
		for (key, value) in &(*me).borrow().subentries {
			for _ in 0..level {
				print!("\t");
			}
			println!("- {key}: {}", (**value).borrow().size);
			Self::print_tree(value, level + 1);
		}
	}

	pub fn recursive_size(me: &Rc<RefCell<Self>>) -> usize {
		let mut size = (*me).borrow().size;
		for entry in (*me).borrow().subentries.values() {
			size += Self::recursive_size(entry);
		}
		size
	}

	pub fn find_pt1_answer(me: &Rc<RefCell<Self>>, mut answers: Vec<usize>) -> Vec<usize> {
		if (*me).borrow().size == 0 {
			// This is a dir
			for entry in (*me).borrow().subentries.values() {
				if (*entry).borrow().size == 0 {
					let size = Self::recursive_size(entry); // Get size of this dir
					if size <= 100_000 {
						answers.push(size);
					}
					answers = Self::find_pt1_answer(entry, answers);
				}
			}
			answers
		} else {
			panic!("Called find_pt1_answer on a file");
		}
	}

	pub fn find_pt2_answer(me: &Rc<RefCell<Self>>, mut answers: Vec<usize>) -> Vec<usize> {
		if (*me).borrow().size == 0 {
			// This is a dir
			for entry in (*me).borrow().subentries.values() {
				if (*entry).borrow().size == 0 {
					let size = Self::recursive_size(entry); // Get size of this dir
					answers.push(size);
					answers = Self::find_pt2_answer(entry, answers);
				}
			}
			answers
		} else {
			panic!("Called find_pt2_answer on a file");
		}
	}
}

pub fn pt1() {
	let input = fs::read_to_string("inputs/day7.txt").unwrap();

	let root = Rc::new(RefCell::new(DirEnt::root()));
	let mut cwd = root.clone();

	// Construct the tree from commands
	for session in input.split("$ ") {
		let mut lines = session.lines();

		let commandline = lines.next().unwrap_or("");

		if commandline.is_empty() {
			continue;
		}

		if commandline.starts_with("cd") {
			let (_, argument) = commandline.split_once(' ').unwrap();
			if argument == "/" {
				cwd = root.clone();
			} else if argument == ".." {
				cwd = DirEnt::get_parent(&cwd);
			} else {
				cwd = DirEnt::get_entry(&cwd, argument);
			}
		} else if commandline.starts_with("ls") {
			for entry in lines {
				let (size, name) = entry.split_once(' ').unwrap();
				if size == "dir" {
					DirEnt::add_entry(&cwd, name, 0);
				} else {
					DirEnt::add_entry(&cwd, name, size.parse().unwrap());
				}
			}
		}
	}

	println!("/");
	DirEnt::print_tree(&root, 1);

	let answer: usize = DirEnt::find_pt1_answer(&root, Vec::new()).iter().sum();
	println!("{answer}");
}

pub fn pt2() {
	let input = fs::read_to_string("inputs/day7.txt").unwrap();

	let root = Rc::new(RefCell::new(DirEnt::root()));
	let mut cwd = root.clone();

	// Construct the tree from commands
	for session in input.split("$ ") {
		let mut lines = session.lines();

		let commandline = lines.next().unwrap_or("");

		if commandline.is_empty() {
			continue;
		}

		if commandline.starts_with("cd") {
			let (_, argument) = commandline.split_once(' ').unwrap();
			if argument == "/" {
				cwd = root.clone();
			} else if argument == ".." {
				cwd = DirEnt::get_parent(&cwd);
			} else {
				cwd = DirEnt::get_entry(&cwd, argument);
			}
		} else if commandline.starts_with("ls") {
			for entry in lines {
				let (size, name) = entry.split_once(' ').unwrap();
				if size == "dir" {
					DirEnt::add_entry(&cwd, name, 0);
				} else {
					DirEnt::add_entry(&cwd, name, size.parse().unwrap());
				}
			}
		}
	}

	//println!("/");
	//DirEnt::print_tree(&root, 1);
	let cleanup_required: usize = 30_000_000 - (70_000_000 - DirEnt::recursive_size(&root));
	let mut answer: Vec<usize> = DirEnt::find_pt2_answer(&root, Vec::new());
	answer.sort_unstable();

	for size in answer {
		if size >= cleanup_required {
			println!("{size:?}");
			return;
		}
	}
}
