use std::fs;

pub fn pt1() {
	let input = fs::read_to_string("inputs/day1.txt").unwrap();

	let mut dwarves = Vec::new();

	for dwarf in input.split("\n\n") {
		let mut total = 0;
		for line in dwarf.lines() {
			total += line.parse::<i32>().unwrap();
		}

		dwarves.push(total);
	}

	dwarves.sort();
	println!("{:?}", dwarves.pop().unwrap());
}

pub fn pt2() {
	let input = fs::read_to_string("inputs/day1.txt").unwrap();

	let mut dwarves = Vec::new();

	for dwarf in input.split("\n\n") {
		let mut total = 0;
		for line in dwarf.lines() {
			total += line.parse::<i32>().unwrap();
		}

		dwarves.push(total);
	}

	dwarves.sort();
	println!("{:?}", dwarves.pop().unwrap() + dwarves.pop().unwrap() + dwarves.pop().unwrap());
}
