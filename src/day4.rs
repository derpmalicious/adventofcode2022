use std::fs;

pub fn pt1() {
	let input = fs::read_to_string("inputs/day4.txt").unwrap();

	let mut contained_count = 0;

	for ((min1, max1), (min2, max2)) in input
		.lines()
		.map(|l| l.split_once(',').unwrap())
		.map(|(a, b)| (a.split_once('-').unwrap(), b.split_once('-').unwrap()))
		.map(|((a, b), (c, d))| {
			(
				(a.parse::<i32>().unwrap(), b.parse::<i32>().unwrap()),
				(c.parse::<i32>().unwrap(), d.parse::<i32>().unwrap()),
			)
		}) {
		if (min1 <= min2 && max1 >= max2) || (min2 <= min1 && max2 >= max1) {
			contained_count += 1;
		}
	}

	println!("{}", contained_count);
}

pub fn pt2() {
	let input = fs::read_to_string("inputs/day4.txt").unwrap();

	let mut contained_count = 0;

	for ((min1, max1), (min2, max2)) in input
		.lines()
		.map(|l| l.split_once(',').unwrap())
		.map(|(a, b)| (a.split_once('-').unwrap(), b.split_once('-').unwrap()))
		.map(|((a, b), (c, d))| {
			(
				(a.parse::<i32>().unwrap(), b.parse::<i32>().unwrap()),
				(c.parse::<i32>().unwrap(), d.parse::<i32>().unwrap()),
			)
		}) {
		if (min1 >= min2 && max2 >= min1) || (min2 >= min1 && max1 >= min2) {
			contained_count += 1;
		}
	}

	println!("{}", contained_count);
}
