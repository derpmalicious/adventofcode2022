use std::fs;

#[derive(Debug)]
enum Ins {
	Noop,
	Addx(i32),
}

pub fn pt1() {
	let input = fs::read_to_string("inputs/day10.txt").unwrap();

	let mut instructions = Vec::new();

	for line in input.lines() {
		if line.starts_with("noop") {
			instructions.push(Ins::Noop);
		} else if line.starts_with("addx") {
			let (_, arg) = line.split_once(' ').unwrap();
			instructions.push(Ins::Noop);
			instructions.push(Ins::Addx(arg.parse().unwrap()));
		}
	}

	let mut answer = 0;

	let mut x = 1;
	for (cycle, instruction) in instructions.iter().enumerate() {
		let cycle = cycle as i32 + 1;

		// State of the current cycle is evaluated before the instruction is executed
		if (cycle - 20) % 40 == 0 {
			println!("c{cycle}: {x}");
			answer += cycle * x;
		}

		// Execute instruction
		match instruction {
			Ins::Noop => {}
			Ins::Addx(v) => x += v,
		}
	}

	println!("{answer}");
}

pub fn pt2() {
	let input = fs::read_to_string("inputs/day10.txt").unwrap();

	let mut instructions = Vec::new();

	for line in input.lines() {
		if line.starts_with("noop") {
			instructions.push(Ins::Noop);
		} else if line.starts_with("addx") {
			let (_, arg) = line.split_once(' ').unwrap();
			instructions.push(Ins::Noop);
			instructions.push(Ins::Addx(arg.parse().unwrap()));
		}
	}

	let mut x = 1;
	for (cycle, instruction) in instructions.iter().enumerate() {
		let cycle = cycle as i32;
		let crt_x = cycle % 40;

		// State of the current cycle is evaluated before the instruction is executed
		if (x - 1..x + 2).contains(&crt_x) {
			print!("#");
		} else {
			print!(".");
		}

		if crt_x == 39 {
			println!();
		}

		// Execute instruction
		match instruction {
			Ins::Noop => {}
			Ins::Addx(v) => x += v,
		}
	}
}
