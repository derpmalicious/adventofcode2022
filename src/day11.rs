use std::cell::RefCell;
use std::fs;
use std::num::ParseIntError;

// Note: I decided to play around with random unneeded bs here for no reason, so the solution is extra shit

#[derive(Debug)]
enum OpArg {
	Val(i32),
	Old,
}

impl TryFrom<&str> for OpArg {
	type Error = ParseIntError;

	fn try_from(value: &str) -> Result<Self, Self::Error> {
		match value {
			"old" => Ok(Self::Old),
			_ => match value.parse() {
				Ok(v) => Ok(Self::Val(v)),
				Err(e) => Err(e),
			},
		}
	}
}

#[derive(Debug)]
enum Op {
	Add(OpArg),
	Mul(OpArg),
	Div(OpArg),
}

impl Op {
	fn calc(&self, arg: i32) -> i32 {
		match self {
			Self::Add(a) => match a {
				OpArg::Val(n) => arg + n,
				OpArg::Old => arg + arg,
			},
			Self::Mul(a) => match a {
				OpArg::Val(n) => arg * n,
				OpArg::Old => arg * arg,
			},
			Self::Div(a) => match a {
				OpArg::Val(n) => arg / n,
				OpArg::Old => 1,
			},
		}
	}

	fn test(&self, arg: i32) -> bool {
		match self {
			Self::Div(a) => match a {
				OpArg::Val(n) => (arg % n) == 0,
				OpArg::Old => true,
			},
			_ => panic!("Invalid test"),
		}
	}
}

impl TryFrom<(&str, &str)> for Op {
	type Error = ();

	fn try_from(value: (&str, &str)) -> Result<Self, Self::Error> {
		let (op, arg) = value;
		let arg: OpArg = arg.try_into().unwrap();

		match op {
			"*" => Ok(Self::Mul(arg)),
			"+" => Ok(Self::Add(arg)),
			"divisible" => Ok(Self::Div(arg)),
			_ => Err(()),
		}
	}
}

#[derive(Debug, Copy, Clone)]
enum Action {
	ThrowTo(i32),
}

#[derive(Debug)]
struct CondAction {
	condition: bool,
	action: Action,
}

impl From<(&str, &str)> for CondAction {
	fn from(value: (&str, &str)) -> Self {
		let (condition, target) = value;

		Self {
			condition: condition.parse().unwrap(),
			action: Action::ThrowTo(target.parse().unwrap()),
		}
	}
}

#[derive(Debug)]
struct Monkey {
	items: RefCell<Vec<i32>>,
	inspect_count: RefCell<i32>,
	op: Op,
	test: Op,
	condactions: Vec<CondAction>,
}

impl Monkey {
	pub fn condition(&self, arg: i32) -> (Action, i32) {
		let eval = self.test.test(arg);
		for cond in &self.condactions {
			if cond.condition == eval {
				return (cond.action, arg);
			}
		}
		panic!("Target not found");
	}

	pub fn give(&self, worry: i32) {
		self.items.borrow_mut().push(worry);
	}

	pub fn inspect_all(&self) -> Vec<(Action, i32)> {
		let mut items = self.items.borrow_mut();
		let len = items.len();

		items
			.drain(0..len)
			.map(|worry| {
				*self.inspect_count.borrow_mut() += 1;
				self.condition(self.op.calc(worry) / 3)
			})
			.collect()
	}
}

pub fn pt1() {
	let input = fs::read_to_string("inputs/day11.txt").unwrap();
	let mut monkeys = Vec::new();

	for monkey in input.split("\n\n") {
		let mut monkey_lines = monkey.lines();
		monkey_lines.next(); // Skip Monkey number line

		let items: Vec<i32> = monkey_lines
			.next()
			.unwrap()
			.split_once(": ")
			.unwrap()
			.1
			.split(", ")
			.map(|n| n.parse().unwrap())
			.collect();

		let op = monkey_lines
			.next()
			.unwrap()
			.split_once("new = old ")
			.unwrap()
			.1
			.split_once(' ')
			.unwrap()
			.try_into()
			.unwrap();

		let test = monkey_lines
			.next()
			.unwrap()
			.split_at(8)
			.1
			.split_once(" by ")
			.unwrap()
			.try_into()
			.unwrap();

		let mut condactions = Vec::new();

		for line in monkey_lines {
			let cond = line.split_at(7).1.split_once(": throw to monkey ").unwrap().into();
			condactions.push(cond);
		}

		let monkeh = Monkey {
			items: RefCell::new(items),
			op,
			test,
			condactions,
			inspect_count: RefCell::new(0),
		};
		monkeys.push(monkeh);
	}

	for _ in 0..20 {
		for monkey in &monkeys {
			let actions = monkey.inspect_all();
			for (action, worry) in actions {
				match action {
					Action::ThrowTo(target) => {
						monkeys[target as usize].give(worry);
					}
				}
			}
		}
	}

	monkeys.sort_by(|a, b| {
		let a = a.inspect_count.borrow_mut();
		let b = b.inspect_count.borrow_mut();
		a.partial_cmp(&*b).unwrap()
	});

	println!(
		"{}",
		*monkeys.pop().unwrap().inspect_count.borrow()
			* *monkeys.pop().unwrap().inspect_count.borrow()
	);
}

pub fn pt2() {
	// Nope.
	// I think you can probably do something with the modulos to keep the number in a sane range, but I don't want to math
}
