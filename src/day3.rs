use std::collections::HashMap;
use std::fs;

pub fn letter_score(a: char) -> i32 {
	if a.is_lowercase() {
		a as i32 - 96
	} else {
		a as i32 - 38
	}
}

pub fn pt1() {
	let input = fs::read_to_string("inputs/day3.txt").unwrap();

	let mut common: Vec<char> = Vec::new();

	for rucksack in input.lines() {
		let mut contents_common = HashMap::new();

		let (compartment_1, compartment_2) = rucksack.split_at(rucksack.len() / 2);

		for (c_1, c_2) in compartment_1.chars().zip(compartment_2.chars()) {
			if compartment_2.contains(c_1) {
				contents_common.insert(c_1, ());
			}
			if compartment_1.contains(c_2) {
				contents_common.insert(c_2, ());
			}
		}

		common.append(&mut contents_common.into_keys().collect());
	}

	let score = common.iter().fold(0, |acc, c| acc + letter_score(*c));

	println!("{}", score);
}

pub fn pt2() {
	let input = fs::read_to_string("inputs/day3.txt").unwrap();

	let mut common: Vec<char> = Vec::new();

	for group in input.lines().array_chunks::<3>() {
		let mut contents_common = HashMap::new();

		for ((c_1, c_2), c_3) in group[0].chars().zip(group[1].chars()).zip(group[2].chars()) {
			if group[1].contains(c_1) && group[2].contains(c_1) {
				contents_common.insert(c_1, ());
			}
			if group[0].contains(c_2) && group[2].contains(c_2) {
				contents_common.insert(c_2, ());
			}
			if group[0].contains(c_3) && group[1].contains(c_3) {
				contents_common.insert(c_3, ());
			}
		}

		common.append(&mut contents_common.into_keys().collect());
	}

	let score = common.iter().fold(0, |acc, c| acc + letter_score(*c));

	println!("{}", score);
}
