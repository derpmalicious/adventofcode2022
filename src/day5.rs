use std::collections::VecDeque;
use std::fs;

pub fn pt1() {
	let input = fs::read_to_string("inputs/day5.txt").unwrap();

	let (crates_str, other_str) = input.split_at(input.find("\n 1").unwrap());
	let (_, moves_str) = other_str.split_once("\n\n").unwrap();

	let mut columns = Vec::new();

	// Create [[]] of crates
	for mut line in crates_str.lines() {
		let mut col_n = 0;
		let mut crte;

		loop {
			let mut last = false;
			col_n += 1;

			if line.len() >= 4 {
				(crte, line) = line.split_at(4);
			} else {
				last = true;
				crte = line;
			}

			if columns.len() < col_n {
				columns.push(VecDeque::new());
			}

			if crte.chars().nth(1).unwrap() != ' ' {
				columns[col_n - 1].push_front(crte.chars().nth(1).unwrap());
			}

			if last {
				break;
			}
		}
	}

	// Execute moves
	for line in moves_str.lines() {
		let (count, tmp) = line.split_at(5).1.split_once(" from ").unwrap();
		let (origin, target) = tmp.split_once(" to ").unwrap();

		let (count, origin, target) = (
			count.parse::<usize>().unwrap(),
			origin.parse::<usize>().unwrap(),
			target.parse::<usize>().unwrap(),
		);

		//println!("{}x {} -> {}", count, origin, target);

		for _ in 0..count {
			let item = columns[origin - 1].pop_back().unwrap();
			columns[target - 1].push_back(item);
		}
	}

	for column in &mut columns {
		print!("{}", column.pop_back().unwrap());
	}
	println!();
}

pub fn pt2() {
	let input = fs::read_to_string("inputs/day5.txt").unwrap();

	let (crates_str, other_str) = input.split_at(input.find("\n 1").unwrap());
	let (_, moves_str) = other_str.split_once("\n\n").unwrap();

	let mut columns = Vec::new();

	// Create [[]] of crates
	for mut line in crates_str.lines() {
		let mut col_n = 0;
		let mut crte;

		loop {
			let mut last = false;
			col_n += 1;

			if line.len() >= 4 {
				(crte, line) = line.split_at(4);
			} else {
				last = true;
				crte = line;
			}

			if columns.len() < col_n {
				columns.push(VecDeque::new());
			}

			if crte.chars().nth(1).unwrap() != ' ' {
				columns[col_n - 1].push_front(crte.chars().nth(1).unwrap());
			}

			if last {
				break;
			}
		}
	}

	// Execute moves
	for line in moves_str.lines() {
		let (count, tmp) = line.split_at(5).1.split_once(" from ").unwrap();
		let (origin, target) = tmp.split_once(" to ").unwrap();

		let (count, origin, target) = (
			count.parse::<usize>().unwrap(),
			origin.parse::<usize>().unwrap(),
			target.parse::<usize>().unwrap(),
		);

		//println!("{}x {} -> {}", count, origin, target);

		let l = columns[origin - 1].len() - count;
		let mut item = columns[origin - 1].split_off(l);
		columns[target - 1].append(&mut item);
	}

	for column in &mut columns {
		print!("{}", column.pop_back().unwrap());
	}
	println!();
}
