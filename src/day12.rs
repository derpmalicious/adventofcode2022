use std::cell::RefCell;
use std::cmp::{max, min};
use std::fs;
use std::rc::Rc;

#[derive(Debug)]
struct Node {
	height: RefCell<u32>,
	visited: RefCell<bool>,
	distance: RefCell<u32>,
	coords: (usize, usize),
}

pub fn pt1() {
	let input = fs::read_to_string("inputs/day12.txt").unwrap();

	let mut grid = Vec::new();
	let mut unvisited = Vec::new(); // I wanted to use BTreeSet, but it seems it can't contain multiple items of same distance? There's probably still a way better way to do this than a vec, but eh
	let mut start = None;
	let mut end = None;
	let mut max_y = 0;
	let mut max_x = 0;
	let mut max_height = 0;

	for (y, line) in input.lines().enumerate() {
		let mut row = Vec::new();
		max_y = y;

		for (x, char) in line.chars().enumerate() {
			max_x = x;
			if char.is_lowercase() {
				max_height = max((char as u8 - b'a') as u32, max_height);
				let node = Rc::new(Node {
					height: RefCell::new((char as u8 - b'a') as u32),
					visited: RefCell::new(false),
					distance: RefCell::new(u32::MAX),
					coords: (x, y),
				});
				unvisited.push(node.clone());
				row.push(node);
			} else {
				match char {
					'S' => {
						let node = Rc::new(Node {
							height: RefCell::new(0),
							visited: RefCell::new(true),
							distance: RefCell::new(0),
							coords: (x, y),
						});
						start = Some(node.clone());
						row.push(node);
					}
					'E' => {
						let node = Rc::new(Node {
							height: RefCell::new(0),
							visited: RefCell::new(false),
							distance: RefCell::new(u32::MAX),
							coords: (x, y),
						});
						end = Some(node.clone());
						unvisited.push(node.clone());
						row.push(node);
					}
					_ => panic!("Invalid char"),
				}
			}
		}
		grid.push(row);
	}

	let start = start.unwrap();
	let end = end.unwrap();

	*end.height.borrow_mut() = max_height;

	let mut visiting = start;

	loop {
		let mut neighbors = Vec::new();

		if visiting.coords.0 > 0 {
			// Left
			neighbors.push((visiting.coords.0 - 1, visiting.coords.1));
		}
		if visiting.coords.0 < max_x {
			// Right
			neighbors.push((visiting.coords.0 + 1, visiting.coords.1));
		}
		if visiting.coords.1 > 0 {
			// Up
			neighbors.push((visiting.coords.0, visiting.coords.1 - 1));
		}
		if visiting.coords.1 < max_y {
			// Down
			neighbors.push((visiting.coords.0, visiting.coords.1 + 1));
		}

		if visiting.coords == end.coords {
			break;
		}

		for neighbor in neighbors {
			let neighbor = &grid[neighbor.1][neighbor.0];
			if !*neighbor.visited.borrow()
				&& *neighbor.height.borrow() <= *visiting.height.borrow() + 1
			{
				let dist = min(*neighbor.distance.borrow(), *visiting.distance.borrow() + 1);
				*neighbor.distance.borrow_mut() = dist;
			}
		}

		*visiting.visited.borrow_mut() = true;

		unvisited.sort_by_key(|x| *x.distance.borrow());
		unvisited.reverse(); // .take_first() plz?

		match unvisited.pop() {
			None => {
				break;
			}
			Some(v) => visiting = v,
		}

		if *visiting.distance.borrow() == u32::MAX {
			break;
		}

		unvisited.reverse();
	}

	/*
	for row in grid {
		for col in row {
			print!("{}\t", col.distance.borrow());
		}
		println!();
	}
	 */

	println!("{}", end.distance.borrow());
}

pub fn pt2() {
	let input = fs::read_to_string("inputs/day12.txt").unwrap();

	let mut grid = Vec::new();
	let mut unvisited = Vec::new(); // I wanted to use BTreeSet, but it seems it can't contain multiple items of same distance? There's probably still a way better way to do this than a vec, but eh
	let mut end = None;
	let mut max_y = 0;
	let mut max_x = 0;
	let mut max_height = 0;

	let mut consider_start = Vec::new();

	for (y, line) in input.lines().enumerate() {
		let mut row = Vec::new();
		max_y = y;

		for (x, char) in line.chars().enumerate() {
			max_x = x;
			if char.is_lowercase() {
				max_height = max((char as u8 - b'a') as u32, max_height);
				let node = Rc::new(Node {
					height: RefCell::new((char as u8 - b'a') as u32),
					visited: RefCell::new(false),
					distance: RefCell::new(u32::MAX),
					coords: (x, y),
				});
				if char == 'a' {
					consider_start.push(node.clone());
				}
				unvisited.push(node.clone());
				row.push(node);
			} else {
				match char {
					'S' => {
						let node = Rc::new(Node {
							height: RefCell::new(0),
							visited: RefCell::new(true),
							distance: RefCell::new(u32::MAX),
							coords: (x, y),
						});
						consider_start.push(node.clone());
						row.push(node);
					}
					'E' => {
						let node = Rc::new(Node {
							height: RefCell::new(0),
							visited: RefCell::new(false),
							distance: RefCell::new(u32::MAX),
							coords: (x, y),
						});
						end = Some(node.clone());
						unvisited.push(node.clone());
						row.push(node);
					}
					_ => panic!("Invalid char"),
				}
			}
		}
		grid.push(row);
	}

	let end = end.unwrap();
	*end.height.borrow_mut() = max_height;
	*end.distance.borrow_mut() = 0;

	let mut visiting = end;

	loop {
		let mut neighbors = Vec::new();

		if visiting.coords.0 > 0 {
			// Left
			neighbors.push((visiting.coords.0 - 1, visiting.coords.1));
		}
		if visiting.coords.0 < max_x {
			// Right
			neighbors.push((visiting.coords.0 + 1, visiting.coords.1));
		}
		if visiting.coords.1 > 0 {
			// Up
			neighbors.push((visiting.coords.0, visiting.coords.1 - 1));
		}
		if visiting.coords.1 < max_y {
			// Down
			neighbors.push((visiting.coords.0, visiting.coords.1 + 1));
		}

		for neighbor in neighbors {
			let neighbor = &grid[neighbor.1][neighbor.0];
			if !*neighbor.visited.borrow()
				&& *visiting.height.borrow() <= *neighbor.height.borrow() + 1
			{
				let dist = min(*neighbor.distance.borrow(), *visiting.distance.borrow() + 1);
				*neighbor.distance.borrow_mut() = dist;
			}
		}

		*visiting.visited.borrow_mut() = true;

		unvisited.sort_by_key(|x| *x.distance.borrow());
		unvisited.reverse(); // .take_first() plz?

		match unvisited.pop() {
			None => {
				break;
			}
			Some(v) => visiting = v,
		}

		if *visiting.distance.borrow() == u32::MAX {
			break;
		}

		unvisited.reverse();
	}

	/*
	for row in grid {
		for col in row {
			print!("{}\t", col.distance.borrow());
		}
		println!();
	}
	 */

	let mut consider_start: Vec<u32> =
		consider_start.iter().map(|start| *start.distance.borrow()).collect();

	consider_start.sort();

	println!("{:?}", consider_start[0]);
}
